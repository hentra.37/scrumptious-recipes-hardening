from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from meal_plans.models import MealPlan
from django.contrib.auth.mixins import LoginRequiredMixin

try:
    from meal_plans.models import MealPlan
except Exception:
    MealPlan = None

class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 2

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context


class MealPlanUpdateView(UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "owner", "description", "image"]
    success_url = reverse_lazy("meal_plans_list")


class MealPlanDeleteView(DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")

class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("meal_plans_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

